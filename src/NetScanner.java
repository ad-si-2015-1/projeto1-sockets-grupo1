package agentesinteligentes;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;


public class NetScanner{
    
    //Porta inicial
    int portaInicial=60000;
    //Porta final
    int portaFinal=60100; 
	  
    //Recebe como parâmetro uma subrede e retorna os IPs e as portas abertas
    public void scann(final String subrede, final Aranha aranha){
        new Thread(){
            @Override public void run() {
                //String que armazena o prefixo da subrede
                String prefixo = subrede.substring(0, subrede.length() - 1);

                //String que armazena o sufixo da subrede
                String sufixo = subrede.substring(subrede.length() - 1);
                //Inteiro que armazena o sufixo da subrede
                int sufixoCliente = Integer.parseInt(sufixo);

                //Loop que percorre cada IP da rede
                for(int j=sufixoCliente; j<=254; j++){
                    try {
                        String sufixoTemp = Integer.toString(j);
                        //String que armazena o endereço atual do cliente
                        String enderecoServidor = prefixo + sufixoTemp;

                        InetAddress ip = InetAddress.getByName(enderecoServidor);
                        System.out.println("scanneando " + enderecoServidor);
                        if(ip.isReachable(5000)){

                            System.out.println("Ip ativo: "+ enderecoServidor);

                            for(int i=portaInicial; i <= portaFinal; i++){
                                System.out.println("verificando porta "+i);
                                //Encontra as portas abertas
                                try{

                                    Socket socket = new Socket(enderecoServidor, i);
                                    System.out.println("PORTA ABERTA: " + i + "\n verificando se é um agente...");

                                    DataOutputStream doStream = new DataOutputStream(socket.getOutputStream());
                                    String pergunta = "AGENTE?";
                                    doStream.writeBytes(pergunta);

                                    BufferedReader buffReader = new BufferedReader( new InputStreamReader(socket.getInputStream()));
                                    String resposta = buffReader.readLine();

                                    ReferenciaAgente agenteEncontrado = new ReferenciaAgente(resposta.getBytes());

                                    aranha.registrarAgenteEncontrado(agenteEncontrado);
                                    //Encerra comunicação com o cliente
                                    socket.close();

                                }
                                //Exceção que pega as portas fechadas
                                catch (Exception e){
                                    System.out.println(e.getMessage());
                                }

                            }
                            //Endereço do cliente que teve as portas verificadas
                            System.out.println("Endereço do cliente: " +enderecoServidor);

                        }
                    } catch (UnknownHostException ex) {
                        Logger.getLogger(NetScanner.class.getName()).log(Level.SEVERE, null, ex);

                    } catch (IOException ex) {
                        Logger.getLogger(NetScanner.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }.start();
    }
    
}
