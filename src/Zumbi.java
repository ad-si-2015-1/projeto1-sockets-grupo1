package agentesinteligentes;


import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Matheus
 */
public class Zumbi extends Agente {
        
    public Zumbi(Endereco enderecoAranha, Endereco enderecoMonitor) throws UnknownHostException, SocketException, IOException {
        super("ZUMBI", enderecoAranha, enderecoMonitor);
    }
    
    public void morder() {
        if(status != "PRESO"){
        } else {
            new Thread() {
                @Override
                public void run() {
                    try {
                        Socket socket = new Socket(referenciaOutro.getEndereco().getIp(), referenciaOutro.getEndereco().getPorta());
                        DataOutputStream doStream = new DataOutputStream(socket.getOutputStream());
                        String mordida = "MORDIDA";
                        doStream.writeBytes(mordida);
                    } catch (IOException ex) {
                        Logger.getLogger(Zumbi.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
            }.start();
        }
    }

    @Override
    void rodarComportamentoEspecifico() {
        morder();
    }

    @Override
    void tratarConexao(Socket conexao) {
        try{
            BufferedReader buffReader = new BufferedReader(new InputStreamReader(conexao.getInputStream()));
            String msgRecebida = buffReader.readLine();
            
            if(msgRecebida.equals("AGENTE?")){
                DataOutputStream doStream = new DataOutputStream(conexao.getOutputStream());
                doStream.writeBytes(new String(getMinhaReferencia().toByteArray()));
            } else if(msgRecebida.equals("PRESO")){
                status = "PRESO";
                notificador.notificarMonitor(getMinhaReferencia());
            }
            
        } catch (IOException ex) {
            Logger.getLogger(Zumbi.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
