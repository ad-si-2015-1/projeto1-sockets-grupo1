import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matheus
 */
public class Cacador extends Agente{
    public void prender() {
        new Thread() {
            @Override
            public void run() {
                Socket socket = new Socket();

                try {
                    DataOutputStream doStream = new DataOutputStream(socket.getOutputStream());
                    String prisao = "PRESO";
                    doStream.writeBytes(prisao);
                } catch (IOException ex) {
                    Logger.getLogger(Zumbi.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }.start();
    }
    
    public Cacador(Endereco enderecoAranha, Endereco enderecoMonitor) throws UnknownHostException {
        super("CACADOR", enderecoAranha, enderecoMonitor);
    }
    
    public void prender() {

        new Thread() {
            @Override
            public void run() {
                try {

                    Socket socket = new Socket(referenciaOutro.getEndereco().getIp(), referenciaOutro.getEndereco().getPorta());
                    DataOutputStream doStream = new DataOutputStream(socket.getOutputStream());
                    String prisao = "PRESO";
                    doStream.writeBytes(prisao);
                } catch (IOException ex) {
                    Logger.getLogger(Cacador.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }.start();

    }
    
    @Override
    void rodarComportamentoEspecifico() {
        prender();
    }

    @Override
    void tratarConexao(Socket conexao) {
        try {
            BufferedReader buffReader = new BufferedReader(new InputStreamReader(conexao.getInputStream()));
            String msgRecebida = buffReader.readLine();
            
            if(msgRecebida.equals("AGENTE?")){
                DataOutputStream doStream = new DataOutputStream(conexao.getOutputStream());
                doStream.writeBytes(new String(getMinhaReferencia().toByteArray()));
            } 
            
        } catch (IOException ex) {
            Logger.getLogger(Cacador.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
