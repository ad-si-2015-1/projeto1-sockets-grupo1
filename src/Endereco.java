package agentesinteligentes;

/**
 *
 * @author Matheus
 */
public class Endereco {
    private String ip;
    private int porta;

    public Endereco(String ip, int porta) {
        this.ip = ip;
        this.porta = porta;
    }

    public Endereco() {
    }
    
    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPorta() {
        return porta;
    }

    public void setPorta(int porta) {
        this.porta = porta;
    }

    @Override
    public String toString() {
        return "Endereco{" + "ip=" + ip + ", porta=" + porta + '}';
    }

}
