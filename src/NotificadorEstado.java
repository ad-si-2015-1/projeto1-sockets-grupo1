package agentesinteligentes;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

/**
 *
 * @author Augusto
 */
public class NotificadorEstado {
    private Endereco enderecoMonitor;

    public NotificadorEstado(Endereco enderecoMonitor) {
        this.enderecoMonitor = enderecoMonitor;
    }

    public void notificarMonitor(ReferenciaAgente referencia) throws SocketException, UnknownHostException, IOException{
        DatagramSocket dgSock = new DatagramSocket();
        InetAddress ipMonitor = InetAddress.getByName( this.enderecoMonitor.getIp() );
        byte[] bReferencia = referencia.toByteArray();
        DatagramPacket pctReferencia = new DatagramPacket(bReferencia, bReferencia.length, ipMonitor, this.enderecoMonitor.getPorta());
        dgSock.send(pctReferencia);
        dgSock.close();
    }
    
    public Endereco getEnderecoMonitor() {
        return enderecoMonitor;
    }

    public void setEnderecoMonitor(Endereco enderecoMonitor) {
        this.enderecoMonitor = enderecoMonitor;
    }
    
    
}
