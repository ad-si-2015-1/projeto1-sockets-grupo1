import java.net.*;
import java.util.Scanner;

public class PortScanner{
    
    public static void main(String args[]){
	  //Porta inicial
	  int portaInicial=0;
	  //Porta final
	  int portaFinal=0; 
	  
	  Scanner sc = new Scanner(System.in);
	  
	  System.out.print("Digite a porta inicial: ");
	  portaInicial = sc.nextInt();
	  System.out.print("Digite a porta final: ");
	  portaFinal = sc.nextInt();
	  
	  //Faixa inicial da subrede a ser varrida
	  String faixaRede = "10.2.0.1";
	  
	  //String que armazena o prefixo da subrede
	  String prefixo = faixaRede.substring(0, faixaRede.length() - 1);
	  
	  //String que armazena o sufixo da subrede
	  String sufixo = faixaRede.substring(faixaRede.length() - 1);
	  //Inteiro que armazena o sufixo da subrede
	  int sufixoCliente = Integer.parseInt(sufixo);
	  
	  //Loop que percorre cada IP da rede
	  for(int j=sufixoCliente; j<=2; j++){
	     
	      String sufixoTemp = Integer.toString(j);
	      //String que armazena o endereço atual do cliente
	      String enderecoCliente = prefixo + sufixoTemp;
	      
	      //Objeto que irá imprimir as portas encontradas
	      StringBuffer portasEncontradas = new StringBuffer();
	      
	      for(int i=portaInicial; i <=portaFinal; i++){
		  //Encontra as portas abertas
		  try{
		      //Criação de socket
		      Socket cliente = new Socket(enderecoCliente, i);
		      
		      //Adiciona na lista as portas abertas encontradas
		      portasEncontradas.append(i+"\n");

                 
		      //Encerra comunicação com o cliente
		      cliente.close();
   
		  }
              
		  //Exceção chamada quando o endereço IP não é encontrado
		  catch (UnknownHostException ur){
		      System.out.println("Endereço inválido");
		  }
              
		  //Exceção que pega as portas fechadas
		  catch (Exception e){
		  }
	      
	      }
	      //Endereço do cliente que teve as portas verificadas
	      System.out.println("Endereço do cliente: " +enderecoCliente);
	      //Lista de portas abertas
	      System.out.println("Portas encontradas: " +portasEncontradas.toString());
	      
	  }
	  
	  

	  
    }
}