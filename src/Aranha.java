/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package agentesinteligentes;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Augusto
 */
public class Aranha {
    private NetScanner netScanner = new NetScanner();
    private Endereco endereco;
    private ReferenciaAgente agenteEncontrado;
    int revisao = 0;

    public Aranha(Endereco endereco) throws UnknownHostException {
        this.endereco = endereco;
        aguardarConexao();
    }

    public Aranha(int porta) throws UnknownHostException {
        this.endereco = new Endereco(InetAddress.getLocalHost().getHostAddress(), porta);
        aguardarConexao();
    }
    
    void registrarAgenteEncontrado(ReferenciaAgente agenteEncontrado) {
        this.agenteEncontrado = agenteEncontrado;
        this.revisao++ ;
    }
    
    private void aguardarConexao(){
        new Thread(){
            @Override public void run() { 
                while(true){
                    try {
                        ServerSocket serverSocket = new ServerSocket(endereco.getPorta());
                        Socket conexao = serverSocket.accept();
                        
                        BufferedReader buffReader = new BufferedReader(new InputStreamReader(conexao.getInputStream()));
                        String msgRecebida = buffReader.readLine();

                        if(msgRecebida.equals("QUALREVISAO?")){
                            DataOutputStream doStream = new DataOutputStream(conexao.getOutputStream());
                            doStream.writeBytes(String.valueOf(revisao));
                        } else if(msgRecebida.equals("AGENTE?")){
                            DataOutputStream doStream = new DataOutputStream(conexao.getOutputStream());
                            doStream.writeBytes(new String(agenteEncontrado.toByteArray()));
                        }

                    } catch (IOException ex) {
                        Logger.getLogger(Agente.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }.start();
    }
    
    void procurarAgentes(){
        netScanner.scann(endereco.getIp(), this);    
    }
    
}
