package agentesinteligentes;

/**
 *
 * @author Augusto
 * 
 * Uma instância dessa classe será passada sempre que um agente for informar
 * seus dados para a aranha, ou a aranha for informar os dados de um agente para
 * outro agente que o requisite.
 * 
 */
public class ReferenciaAgente {
    private Endereco endereco;
    private String tipo;
    private String status;

    public ReferenciaAgente(Endereco endereco, String tipo, String status) {
        this.endereco = endereco;
        this.tipo = tipo;
        this.status = status;
    }

    ReferenciaAgente(byte[] byteArrayRef) throws NaoAgenteException {
        if(new String(byteArrayRef).substring(0, 9).equals("REFAGENTE")){
            int ipLen        = byteArrayRef[9];
            int posIp        = 10;
            
            int posPortaLen  = 9 + ipLen + 1;
            int portaLen     = byteArrayRef[posPortaLen];
            int posPorta    = posPortaLen + 1;
            
            int posTipoLen   = posPortaLen + portaLen + 1;
            int tipoLen             = byteArrayRef[posTipoLen];
            int posTipo     = posTipoLen + 1;

            int posStatusLen = posTipoLen + tipoLen + 1;
            int statusLen    = byteArrayRef[posStatusLen];
            int posStatus   = posStatusLen + 1; 
            
            byte[] bytesIp = new byte[ipLen];
            System.arraycopy(byteArrayRef, posIp, bytesIp, 0, ipLen);
            String ip = new String(bytesIp);
            
            byte[] bytesPorta = new byte[portaLen];
            System.arraycopy(byteArrayRef, posPorta, bytesPorta, 0, portaLen);
            int porta = Integer.parseInt(new String(bytesPorta));
            
            this.endereco = new Endereco(ip, porta);
            
            byte[] bytesTipo = new byte[tipoLen];
            System.arraycopy(byteArrayRef, posTipo, bytesTipo, 0, tipoLen);
            this.tipo = new String(bytesTipo);
            
            byte[] bytesStatus = new byte[statusLen];
            System.arraycopy(byteArrayRef, posStatus, bytesStatus, 0, statusLen);
            this.status = new String(bytesStatus);
            
        } else{
            throw new NaoAgenteException();
        }
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public byte[] toByteArray() {
        int refagenteLen    = "REFAGENTE".length();
        byte[] bRefagente   = "REFAGENTE".getBytes();
        
        int ipLen           = this.endereco.getIp().length();
        byte[] bIp          = this.endereco.getIp().getBytes();
        
        int portaLen        = String.valueOf(this.endereco.getPorta()).length();
        byte[] bPorta       = String.valueOf(this.endereco.getPorta()).getBytes();
        
        int tipoLen         = this.tipo.length();
        byte[] bTipo        = this.tipo.getBytes();
        
        int statusLen       = this.status.length();
        byte[] bStatus      = this.status.getBytes();
        
        byte[] bRef = new byte[ refagenteLen 
                                + 1 + ipLen 
                                + 1 + portaLen
                                + 1 + tipoLen
                                + 1 + statusLen
                               ];
        
        System.arraycopy(bRefagente, 0, bRef, 0, refagenteLen);
        
        int posIpLen = refagenteLen;
        bRef[posIpLen] = (byte) ipLen;
        System.arraycopy(bIp, 0, bRef, posIpLen+1, ipLen);
        
        int posPortaLen = posIpLen + ipLen + 1;
        bRef[posPortaLen] = (byte) portaLen;
        System.arraycopy(bPorta, 0, bRef, posPortaLen + 1, portaLen);
        
        int posTipoLen = posPortaLen + portaLen + 1;
        bRef[posTipoLen] = (byte) tipoLen;
        System.arraycopy(bTipo, 0, bRef, posTipoLen + 1, tipoLen);
        
        int posStatusLen = posTipoLen + tipoLen + 1;
        bRef[posStatusLen] = (byte) statusLen;
        System.arraycopy(bStatus, 0, bRef, posStatusLen + 1, statusLen);
        
        return bRef;
    }

    @Override
    public String toString() {
        return "ReferenciaAgente{" + "endereco=" + endereco.toString() + ", tipo=" + tipo + ", status=" + status + '}';
    }
    
    
}
