
package agentesinteligentes;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Augusto
 */
public abstract class Agente {
    Endereco endereco;
    Endereco enderecoAranha;
    int numRevisaoAranha = 0; 
    ReferenciaAgente minhaReferencia;
    ReferenciaAgente referenciaOutro;
    NotificadorEstado notificador;
    String tipo;
    String status;

    public Agente(String tipo, Endereco enderecoAranha, Endereco enderecoMonitor) throws UnknownHostException, SocketException, IOException {
        this.enderecoAranha = enderecoAranha;
        
        //verifica ip da maquina em que esta rodando
        String ip = InetAddress.getLocalHost().getHostAddress();
        
        //gera uma porta aleatoriamente, entre 60000 e 65000
        Random random = new Random();
        int porta = random.nextInt(100) + 60000;
        
        //inicializa o endereço com ip e porta acima
        this.endereco = new Endereco(ip, porta);
        
        this.status = "NORMAL";
        
        System.out.println("Agente tipo " + this.tipo + ". " + endereco.toString());
        
        monitorarAranha();
        aguardarConexao();
        
        this.notificador = new NotificadorEstado(enderecoMonitor);
        this.notificador.notificarMonitor(getMinhaReferencia());
        
    }
    
    ReferenciaAgente getMinhaReferencia(){
        return new ReferenciaAgente(this.endereco, this.tipo, this.status);
    }
    
    abstract void rodarComportamentoEspecifico();
    
    public void monitorarAranha(){
        new Thread(){
            @Override public void run() { 
                while(true){
                    try {
                        Socket socket = new Socket(enderecoAranha.getIp(), enderecoAranha.getPorta());

                        DataOutputStream doStream = new DataOutputStream(socket.getOutputStream());
                        String pergunta = "QUALREVISAO?";
                        doStream.writeBytes(pergunta);

                        BufferedReader buffReader = new BufferedReader( new InputStreamReader(socket.getInputStream()));
                        String resposta = buffReader.readLine();

                        int revisaoAtual = Integer.parseInt(resposta);

                        if(revisaoAtual > numRevisaoAranha){
                            numRevisaoAranha = revisaoAtual;
                            pergunta = "AGENTE?";
                            doStream.writeBytes(pergunta);

                            resposta = buffReader.readLine();
                            referenciaOutro = new ReferenciaAgente(resposta.getBytes());
                            rodarComportamentoEspecifico();
                        }
                        
                        Thread.sleep(20000);

                        socket.close();
                        
                    } catch (UnknownHostException ex) {
                        Logger.getLogger(Agente.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        Logger.getLogger(Agente.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (NaoAgenteException ex) {
                        Logger.getLogger(Agente.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Agente.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } 
            } 

        }.start();
    }
   
    private void aguardarConexao(){
        new Thread(){
            @Override public void run() { 
                while(true){
                    try {
                        ServerSocket serverSocket = new ServerSocket(endereco.getPorta());
                        Socket conexao = serverSocket.accept();
                        tratarConexao(conexao);
                    } catch (IOException ex) {
                        Logger.getLogger(Agente.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }.start();
    }
   
    abstract void tratarConexao(Socket conexao);
   
}
